from django.urls import path
from . import views

urlpatterns = [
    # ex: /cars/
    path('', views.index, name='index'),
    # ex: /cars/5/
    path('<int:question_id>/', views.detail, name='detail'),
    # ex: /cars/5/results/
    path('<int:question_id>/results/', views.results, name='results'),
    # ex: /cars/5/vote/
    path('<int:question_id>/vote/', views.vote, name='vote'),
]