from django.urls import path
from . import views
from . import models

urlpatterns = [
    # ex: /cars/
    path('', views.index, name='index'),
    path('<Car:selectedCar>/', views.detail, name='detail'),
]