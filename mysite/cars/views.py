from django.shortcuts import render
from django.http import HttpResponse

from .models import Car


# Create your views here.
def index(request):
    cars_list = Car.objects.all()
    context = {'cars_list': cars_list}
    return render(request, 'cars/index.html', context)

def detail(request, selectedCar):
    context = {'car': selectedCar}
    return HttpResponse(request, 'car/carDetails.html', context)
