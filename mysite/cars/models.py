import datetime
from django.conf import settings
from django.db import models
from django.utils import timezone

# Create your models here.
class Car(models.Model):
    marque = models.CharField(max_length=50)
    modele = models.CharField(max_length=100)
    construct_year = models.IntegerField(default=2000)
    cylindres = models.IntegerField(default=200)
    version = models.CharField(max_length=100)

    def __str__(self):
        return self.marque

class CarRatings(models.Model):
    rater = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    rate = models.IntegerField(default=0)
    carRated = models.ForeignKey(Car, on_delete=models.CASCADE)

    def __str__(self):
        return self.rater.username, self.carRated.marque



